import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Counter, {a, b} from './components/Counter'
import Sum from './components/Sum'

console.log(a);
console.log(b);

class Describe extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    console.log('Desribe.render()');
    return (
      <div>
        <div style={{ color: 'red' }}>Describe: { this.props.desc }</div>
      </div>
    )
  }
}

class Examples extends Component {
  constructor(props) {
    super(props)
    this.state = { describe: 'none' }
    this.updateDescribe = this.updateDescribe.bind(this);
  }

  updateDescribe(describe) {
    this.setState({ describe: describe });
  }


  render() {
    console.log('Examples.render()');
    return (
      <div>
        <h1>React</h1>
        <Counter descCounter={ this.updateDescribe }/>
        <Sum descSum={ this.updateDescribe }/>
        <Describe desc={ this.state.describe }/>
      </div>
    )
  }
}

ReactDOM.render(
  <Examples/>,
  document.getElementById('root')  
)
