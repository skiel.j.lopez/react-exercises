import React, { Component } from 'react'
import ReactDOM from 'react-dom'
//import { createStore } from 'redux'
//import Counter from './components/Counter'
//import counter from './reducers'

//const store = createStore(counter)
//const rootEl = document.getElementById('root')

class Examples extends Component {
  func01 = () => {
    console.log('Hello from func01');
    return 'Function 01';
  }

  render() {
    // This is JSX code not html
    // JSX is a combination between HTML and JS
    // We use curly brices to inject js code into html
    return (
      <div>
        <div>Simple React</div>
        <div style={{ color: "red" }}>Hello World</div>
        <div>{ 1 + 1 }</div>
        <div>{ this.func01() }</div>
      </div>
    )
  }
}

ReactDOM.render(
  <Examples/>,
  document.getElementById('root')
)

//render()
//store.subscribe(render)
