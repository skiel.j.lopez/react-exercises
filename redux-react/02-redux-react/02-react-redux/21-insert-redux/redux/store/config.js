import { createStore, applyMiddleware } from 'redux'
import combineReducer from '../reducers/index'
import { logger, crashReporter, thunk } from '../middlewares/index'

const store = createStore(
  combineReducer,
  // The order in wich the middlewares are invoked matters. The last one when
  // executing next(action) will be passing the acction to the reducer.
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(
    logger,         // first middleware
    crashReporter,  // second middleware
    thunk           // third middleware
  )
);
export default store;
