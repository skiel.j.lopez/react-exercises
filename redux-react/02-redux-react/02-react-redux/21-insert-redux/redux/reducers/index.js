import counter from './counter'
import randomImages from './randomImages'
import sum from './sum'

export default function combineReducer(currentState, action) {
  var nextState = Object.assign({}, currentState);

  nextState = {
    count: counter(nextState.count, action),
    sum: sum(nextState.sum, action),
    images: randomImages(nextState.images, action)
  };

  return nextState;
};
