// Sub Reducer. Call came from Combine reducer
export default function randomImages(state = { data: [], loading: 'Please click the "Random Images" button' }, action) {
  switch (action.type) {
    case 'SHOW_IMAGES':
      return Object.assign(
        {},
        state,
        {
          data: action.data,
          loading: 'Loaded'
        }
      );
    case 'GETTING_IMAGES':
      return Object.assign(
        {},
        state,
        { loading: 'Loading ...' }
      );
    default:
      return state
  };
};
