import $ from 'jquery'

export default function randomImages(cb) {
  var imgurAPI = 'https://api.imgur.com/3/gallery/random/random/1'
  var request = $.ajax({
    method: 'GET',
    url: imgurAPI,
    headers: {
      authorization: 'Client-ID 0190fd523eecabd',
    },
    dataType: 'json'
  });
  
  request.done(data => cb(data.data));

  request.fail(err => cb(err));
}
