import randomImages from '../_imgurAPI'

export const decrease = () => ( { type: 'DECREMENT' } );

export const getSum = (a, b) => ( { type: 'SUM', a: parseInt(a), b: parseInt(b) } );

export const getRandomImages = (dispatch, state) => {
  dispatch({ type: 'GETTING_IMAGES' });
  randomImages(
    ret => dispatch({ type: 'SHOW_IMAGES', data: ret })
  );
};
