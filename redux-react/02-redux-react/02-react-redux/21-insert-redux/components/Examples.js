import React, { Component } from 'react'
import Counter2, {a, b} from './Counter'
import Sum from './Sum'
import RandomImages2 from './RandomImages'

export default class Examples extends Component {
  constructor(props) {
    super(props)
    this.state = { describe: 'none' }
    this.updateDescribe = this.updateDescribe.bind(this);
  }

  updateDescribe(describe) {
    this.setState({ describe: describe });
  }


  render() {
    console.log('Examples.render()');
    return (
      <div>
        <h1>React</h1>
        <Counter2
          descCounter={ this.updateDescribe }
          count={this.props.value.count}
          onDecrement={this.props.onDecrement}
        />
        <Sum
          descSum={ this.updateDescribe }
          sum={this.props.value.sum}
          onSum={this.props.onSum}
        />
        <RandomImages2
          images={this.props.value.images}
          onRandomImages={this.props.onRandomImages}
        />
      </div>
    )
  }
}
