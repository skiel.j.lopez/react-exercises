import React, { Component } from 'react'
import Counter2, {a, b} from './Counter2'
import Sum2 from './Sum2'
import RandomImages2 from './RandomImages2'

export default class Examples2 extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    console.log('Examples.render()');
    return (
      <div>
        <h1>React</h1>
        <Counter2
          count={this.props.value.count}
          onDecrement={this.props.onDecrement}
        />
        <Sum2
          sum={this.props.value.sum}
          onSum={this.props.onSum}
        />
        <RandomImages2
          images={this.props.value.images}
          onRandomImages={this.props.onRandomImages}
        />
      </div>
    )
  }
}
