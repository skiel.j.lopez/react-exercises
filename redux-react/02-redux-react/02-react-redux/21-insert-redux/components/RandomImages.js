import React, { Component } from 'react'
import $ from 'jquery'
// You can only use export default once in each file.
export default class RandomImages extends Component {
  constructor(props) {
    super(props)
    this.state = { 
      data: [],
      loading: 'Please click the "Random Images" button'
    }
    this.randomImages = this.randomImages.bind(this);
  }

  randomImages() {
    this.setState({loading: 'loading ...'});
    var imgurAPI = 'https://api.imgur.com/3/gallery/random/random/1'
    var request = $.ajax({
      method: 'GET',
      url: imgurAPI,
      headers: {
        authorization: 'Client-ID 0190fd523eecabd',
      },
      dataType: 'json'
    });
    
    request.done(
      data => this.setState({
        data: data.data.map(
          data => <img key={ data.id }
                       src={ data.link }
                       alt="imgur image"
                       style={{ height: "200px" }}/>
        ),
        loading: 'Loaded'
      })
    );

    request.fail(err => cb(err));
  }

  render() {
    console.log('RandomImages.render()');
    //stateless function
    function ImageList(props) {
      return(
        <div>
          { props.images }
        </div>
      )
    }
    return (
      <div>
        <button onClick={ this.randomImages }>Random Images</button>
        <br/><br/>CountStatus:
        <span style={{ color: "blue" }}>{ this.state.loading }</span>
        <ImageList images={ this.state.data }/>
      </div>
    )
  }
}
