import React, { Component } from 'react'
// You can only use export default once in each file.
export default class RandomImages2 extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div>
        <button onClick={ this.props.onRandomImages }>Random Images</button>
        <br/><br/>CountStatus:
        <span style={{ color: "blue" }}>{ this.props.images.loading }</span>
        <ImageList images={ this.props.images.data }/>
      </div>
    )

    //stateless function
    function ImageList(props) {
      return(
        <div>
          { 
            props.images.map(
              data => <img key={ data.id }
                           src={ data.link }
                           alt="imgur image"
                           style={{ height: "200px" }}/>
            )
          }
        </div>
      )
    }
  }
}
