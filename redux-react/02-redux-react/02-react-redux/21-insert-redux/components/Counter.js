import React, { Component } from 'react'
// You can only use export default once in each file.
export default class Counter extends Component {
  constructor(props) {
    super(props)
    this.state = { count: 0 }
    this.decrease = this.decrease.bind(this);
  }

  decrease() {
    this.setState({count: this.state.count - 1});
    this.props.descCounter('Click "decrease" button')
  }

  render() {
    console.log('Counter.render()');
    return (
      <div>
        Clicked: <span>{ this.state.count }</span> times
        <button onClick={ this.decrease }>Decrease</button>
        <p/>
      </div>
    )
  }
}

// This type of export can be imported in curly braces on the main js
// You can use const, let or var
export const a = 'A';
export const b = { b: 'B' };
