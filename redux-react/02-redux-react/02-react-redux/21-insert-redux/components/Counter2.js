import React, { Component } from 'react'
// You can only use export default once in each file.
export default class Counter2 extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div>
        Clicked: <span>{ this.props.count.result }</span> times
        <button onClick={ this.props.onDecrement }>Decrease</button>
        <p/>
      </div>
    )
  }
}

// This type of export can be imported in curly braces on the main js
// You can use const, let or var
export const a = 'A';
export const b = { b: 'B' };
