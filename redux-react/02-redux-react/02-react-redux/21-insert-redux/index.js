import React from 'react'
import ReactDOM from 'react-dom'
import Examples2 from './components/Examples2'
import store from './redux/store/config'
import {decrease, getSum, getRandomImages} from './redux/actions/creators'

function render() {
  console.log(store.getState())
  ReactDOM.render(
    <Examples2
      value={store.getState()}
      onDecrement={() => store.dispatch(decrease())}
      onSum={(a,b) => store.dispatch(getSum(a,b))}
      onRandomImages={() => store.dispatch(getRandomImages)}
    />,
    document.getElementById('root')  
  )
}
render();
store.subscribe(render);
