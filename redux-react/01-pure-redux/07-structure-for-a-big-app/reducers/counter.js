// Sub Reducer
function counter(currentState, action) {
  var DEFAULT_STATE = 0;
  var nextState = currentState;

  if (nextState === undefined) {
    nextState = DEFAULT_STATE;
  } else if (action.type === 'DECREMENT') {
    nextState = currentState - 1;
  } else if (action.type === 'INCREMENT'){
    nextState = currentState + 1;
  }

  return nextState;
};
