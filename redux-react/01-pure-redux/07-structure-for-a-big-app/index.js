// STEP 1.2
var store = Redux.createStore(combineReducer);
// END 1.2
function render() {
  var state = store.getState();
  document.getElementById('value').innerHTML = state.count;
  document.getElementById('value2').innerHTML = state.sum;
}
// Invokes render everytime the state gets updated
store.subscribe(render);

document.getElementById('decrement')
  .addEventListener('click', function () {
      // store.dispatch sends the action to the reducer (counter)
      store.dispatch(decrease());
  })

document.getElementById('incrementAsync')
  .addEventListener('click', function () {
    setTimeout(
      function increment () {
        store.dispatch(increase());
      }
    , 1000); // execute increment () method after 1000 millisecond or 1 second
  })

// Excercise 2
document.getElementById('sum')
  .addEventListener('click', function() {
    var a = document.getElementById('a').value;
    var b = document.getElementById('b').value;
    store.dispatch(getSum(a, b));
  });
