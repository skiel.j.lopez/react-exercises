var _fakeServerApi = {
  increaseCount: function(currentCount, cb) {
    setTimeout(function() {
        cb(currentCount + 1);
    }, 5000); // execute increment () method after 1000 millisecond or 1 second
  }
};
