var decrease = function() {
  return { type: 'DECREMENT' };
};

var increase = function() {
  return { type: 'INCREMENT' };
};

var getSum = function(a, b) {
  return { type: 'SUM', a: parseInt(a), b: parseInt(b) };
};

// ASYNC
var asyncIncrease = function(dispatch, state) {
  dispatch({ type: 'INCREMENT_LOADING' });
  _fakeServerApi.increaseCount(
    state.count.result,
    function(data) {
      dispatch({ type: 'INCREMENT', result: data });
    }
  );
};

var getRandomImages = function(dispatch, state) {
  dispatch({ type: 'GETTING_IMAGES' });
  _imgurAPI.getRandomImages(
    function(ret) {
      dispatch({ type: 'SHOW_IMAGES', data: ret });
    }
  );
};
