// Sub Reducer
function sum(currentState, action) {
  var DEFAULT_STATE = 0;
  var nextState = currentState;

  if (currentState === undefined) {
    nextState = DEFAULT_STATE;
  } else if (action.type === 'SUM') {
    nextState = action.a + action.b;
    //funcWithError();
  }

  return nextState;
};

function funcWithError() {
  throw Error('An error for sum')
};
