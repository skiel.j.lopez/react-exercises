// STEP 1.2
var store = Redux.createStore(
  combineReducer,
  // The order in wich the middlewares are invoked matters. The last one when
  // executing next(action) will be passing the acction to the reducer.
  Redux.applyMiddleware(
    logger,         // first middleware
    crashReporter,  // second middleware
    thunk           // third middleware
  )
);
// END 1.2
function render() {
  var state = store.getState();
  document.getElementById('value').innerHTML = state.count.result;

  if(state.count.loading) {
    document.getElementById('status').innerHTML = 'Is Loading ...';
  } else {
    document.getElementById('status').innerHTML = 'Loaded';
  };
  
  document.getElementById('value2').innerHTML = state.sum;
};
// Invokes render everytime the state gets updated
render();
store.subscribe(render);

document.getElementById('decrement')
  .addEventListener('click', function () {
      // store.dispatch sends the action to the reducer (counter)
      store.dispatch(decrease());
  });

document.getElementById('incrementAsync')
  .addEventListener('click', function () {
    store.dispatch(asyncIncrease);
    //setTimeout(
    //  function increment () {
    //    store.dispatch(increase());
    //  }
    //, 1000); // execute increment () method after 1000 millisecond or 1 second
  });

// Excercise 2
document.getElementById('sum')
  .addEventListener('click', function() {
    var a = document.getElementById('a').value;
    var b = document.getElementById('b').value;
    store.dispatch(getSum(a, b));
  });
