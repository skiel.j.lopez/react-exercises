// Sub Reducer
var sum = (state = 0, action) => {
  switch (action.type) {
    case 'SUM':
      return action.a + action.b;
    default:
      return state;
  };
};
