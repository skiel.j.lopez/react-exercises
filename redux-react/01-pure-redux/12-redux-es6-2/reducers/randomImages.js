// Sub Reducer. Call came from Combine reducer
var randomImages = (state = { data: [], loading: false }, action) => {
  switch (action.type) {
    case 'SHOW_IMAGES':
      return Object.assign(
        {},
        state,
        {
          data: action.data,
          loading: false
        }
      );
    case 'GETTING_IMAGES':
      return Object.assign(
        {},
        state,
        { loading: true }
      );
    default:
      return state
  };
};
