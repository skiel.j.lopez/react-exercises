const _imgurAPI = {
  // client ID    : 0190fd523eecabd
  // client secret: 3e304715cba6aa5d35e7f8c2eb480d97bd4142f6
  getRandomImages: cb => {
    var imgurAPI = 'https://api.imgur.com/3/gallery/random/random/1'
    var request = $.ajax({
      method: 'GET',
      url: imgurAPI,
      headers: {
        authorization: 'Client-ID 0190fd523eecabd',
      },
      dataType: 'json'
    });
    
    request.done(
      data => cb(
        data.data.map(
          data => '<img src="'+data.link+'" alt="imgur image" style="height: 200px;">'
        )
      )
    );

    request.fail(err => cb(err));
  }
}
