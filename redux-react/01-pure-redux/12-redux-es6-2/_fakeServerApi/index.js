// execute increment () method after 1000 millisecond or 1 second
const _fakeServerApi = {
  increaseCount: (currentCount, cb) => setTimeout(() => cb(currentCount + 1), 5000)
};
