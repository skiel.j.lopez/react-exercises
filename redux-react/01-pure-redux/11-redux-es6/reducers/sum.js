// Sub Reducer
var sum = (currentState, action) => {
  var DEFAULT_STATE = 0;
  var nextState = currentState;

  if (currentState === undefined) {
    nextState = DEFAULT_STATE;
  } else if (action.type === 'SUM') {
    nextState = action.a + action.b;
  }

  return nextState;
};
