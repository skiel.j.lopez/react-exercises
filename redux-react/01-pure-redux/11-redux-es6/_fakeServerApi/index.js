// execute increment () method after 1000 millisecond or 1 second
var _fakeServerApi = {
  increaseCount: (currentCount, cb) => setTimeout(() => cb(currentCount + 1), 5000)
};
