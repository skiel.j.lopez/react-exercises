var decrease = () => ( { type: 'DECREMENT' } );
var increase = () => ( { type: 'INCREMENT' } );
var getSum = (a, b) => ( { type: 'SUM', a: parseInt(a), b: parseInt(b) } );

// ASYNC
var asyncIncrease = (dispatch, state) => {
  dispatch({ type: 'INCREMENT_LOADING' });
  _fakeServerApi.increaseCount(
    state.count.result,
    data => dispatch({ type: 'INCREMENT', result: data })
  );
};

var getRandomImages = (dispatch, state) => {
  dispatch({ type: 'GETTING_IMAGES' });
  _imgurAPI.getRandomImages(
    ret => dispatch({ type: 'SHOW_IMAGES', data: ret })
  );
};
