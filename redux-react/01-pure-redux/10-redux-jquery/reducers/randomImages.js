// Sub Reducer. Call came from Combine reducer
function randomImages(currentState, action) {
  var DEFAULT_STATE = { data: [], loading: false };
  var nextState = Object.assign({}, currentState);

  if (currentState === undefined) {
    nextState = DEFAULT_STATE;
  } else if (action.type === 'SHOW_IMAGES') {
    console.log(action.data);
    nextState.data = action.data;
    nextState.loading = false;
  } else if (action.type === 'GETTING_IMAGES') {
    nextState.loading = true;
  }

  return nextState;
};
