// STEP 2.2 Main reducer
function combineReducer(currentState, action) {
  var nextState = Object.assign({}, currentState);
  nextState = {
    count: counter(nextState.count, action),
    sum: sum(nextState.sum, action),
    images: randomImages(nextState.images, action)
  };

  return nextState;
};
// END 2.2
