// Sub Reducer
function counter(currentState, action) {
  var DEFAULT_STATE = { result: 0, loading: false };
  var nextState = Object.assign({}, currentState);

  if (currentState === undefined) {
    nextState = DEFAULT_STATE;
  } else if (action.type === 'DECREMENT') {
    nextState.result = currentState.result - 1;
  } else if (action.type === 'INCREMENT'){
    nextState.result = action.result;
    nextState.loading = false;
  } else if (action.type === 'INCREMENT_LOADING'){
    nextState.loading = true;
  }

  return nextState;
};
