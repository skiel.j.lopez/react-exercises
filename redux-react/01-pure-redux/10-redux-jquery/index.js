// STEP 1.2
var store = Redux.createStore(
  combineReducer,
  // The order in wich the middlewares are invoked matters. The last one when
  // executing next(action) will be passing the acction to the reducer.
  Redux.applyMiddleware(
    logger,         // first middleware
    crashReporter,  // second middleware
    thunk           // third middleware
  )
);
// END 1.2
function render() {
  var state = store.getState();
  $('#value').text(state.count.result);

  // Increment status
  if(state.count.loading) {
    $('#status').text('Is Loading ...');
  } else {
    $('#status').text('Loaded');
  };

  $('#value2').text(state.sum);

  // Random images status
  if(state.images.loading) {
    $('#imagesStatus').text('Is Loading ...');
  } else {
    $('#imagesStatus').text('Loaded');
  };

  $('#imagesList').empty();
  $('#imagesList').append(state.images.data.join(''));
};
// Invokes render everytime the state gets updated
render();
store.subscribe(render);

$('#decrement').on('click', function() {
  // store.dispatch sends the action to the reducer (counter)
  store.dispatch(decrease());
});

$('#incrementAsync').on('click', function() {
  store.dispatch(asyncIncrease);
});

// Excercise 2
$('#sum').on('click', function() {
  var a = document.getElementById('a').value;
  var b = document.getElementById('b').value;
  store.dispatch(getSum(a, b));
});

// Excercise 09
$('#randomImagesButton').on('click', function () {
  store.dispatch(getRandomImages);
});
