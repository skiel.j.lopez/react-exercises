var _imgurAPI = {
  // client ID    : 0190fd523eecabd
  // client secret: 3e304715cba6aa5d35e7f8c2eb480d97bd4142f6
  getRandomImages: function(cb) {
    var imgurAPI = 'https://api.imgur.com/3/gallery/random/random/1'
    var request = $.ajax({
      method: 'GET',
      url: imgurAPI,
      headers: {
        authorization: 'Client-ID 0190fd523eecabd',
      },
      dataType: 'json'
    });
    
    request.done(function(data) {
      cb(data.data.map(function(data) {
          return '<img src="'+data.link+'" alt="imgur image" style="height: 200px;">';
      }));
    });

    request.fail(function(err) {
      cb(err);
    });
  }
}
